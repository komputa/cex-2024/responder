import {createServer} from "node:http";
import {DataGenerator} from "./utils/DataGenerator";
import {ResponseDelayer} from "./utils/ResponseDelayer";
import {LogEntry} from "./utils/logger/LogEntry";

const port = process.env.PORT ?? 3000;

const server = createServer((req, res) => {
    const logEntry = new LogEntry();
    logEntry.setRequestReceived();
    console.log('Got request for: ' + req.url);

    if (req.url !== "/respond") {
        const notFound = "404 Not Found";
        res.writeHead(404, {
            "Content-Type": "text/plain",
            "Content-Length": notFound.length
        });
        res.write(notFound);
        res.end();
        return;
    }

    const randomDataGenerator = new DataGenerator();
    const delayer = new ResponseDelayer();

    delayer.delay(() => {
        const randomData = randomDataGenerator.generate();

        res.writeHead(200, {
            "Content-Type": "text/plain",
            "Content-Length": randomData.length,
        });
        res.write(randomData);
        res.end();

        logEntry.setResponseT();
        logEntry.setBodySize(randomData.length);
        console.log('Logging String: ' + logEntry.toString());
    });
});

console.table({
    port,
});

server.listen(port, () => {
    console.log("Server is running on port " + port);
});

process.on('SIGINT', function() {
    console.log("Caught interrupt signal, exiting...");
    process.exit();
});

process.on('SIGTERM', function() {
    console.log("Caught interrupt signal, exiting...");
    process.exit();
});

export class LogEntry {
    // [<requestReceivedT>]|[<responseT>]|<duration>|<responseBodySize>
    private requestReceivedT: number = 0;
    private responseT: number = 0;
    private bodySize: number = 0;

    setRequestReceived(requestReceivedT: number = Date.now()) {
        this.requestReceivedT = requestReceivedT;
    }

    setResponseT(responseT: number = Date.now()) {
        this.responseT = responseT;
    }

    setBodySize(size: number) {
        this.bodySize = size;
    }

    toString() {
        return `[${this.requestReceivedT}]|[${this.responseT}]|[${this.responseT - this.requestReceivedT}]|[${this.bodySize}]`;
    }
}

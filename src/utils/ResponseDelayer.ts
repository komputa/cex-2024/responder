export class ResponseDelayer {

    delay(callback: () => void) {
        setTimeout(callback, this.getDelay());
    }

    /**
     * ten seconds in milliseconds
     * @private
     */
    private getDelay() {
        return Math.random() * 1000 * 10;
    }
}

export class DataGenerator {
    static characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    generate(): string {
        let result = '';
        let length = this.byteLength();

        for (let i = 0; i < length; i++) {
            result += this.randomChar();
        }

        return result;
    }

    /**
     * Number of bytes 0Bytes to 1MB
     * @private
     */
    private byteLength() {
        return Math.random() * 1024 * 1024;
    }

    private randomChar() {
        return DataGenerator.characters.charAt(
            Math.floor(Math.random() * DataGenerator.characters.length)
        );
    }
}
